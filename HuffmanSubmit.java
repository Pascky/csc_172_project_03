// Import any package as required
import java.util.*;
import java.io.*;



public class HuffmanSubmit implements Huffman {
  
	// Feel free to add more methods and variables as required.
	
	/* Used to construct tree */
	class HNode{
		char c;
		int freq;
		HNode left;
		HNode right;
		public HNode(char c, int freq, HNode left, HNode right){
			this.c = c;
			this.freq = freq;
 			this.left = left;
			this.right = right;
		} 

		public boolean isLeaf(){
			return ((left == null) && (right == null));
		}

	}

	public int depth(HNode root){
		if(root == null){
			return 0;
		}
		if(root.isLeaf()){
			return 0;
		}
		return Math.max(1+depth(root.left), 1+depth(root.right));
	}

	public void printLevelOrder(HNode root){
	        // Base Case
	        if(root == null)
	            return;  
	        // Create an empty queue for level order tarversal
	        Queue<HNode> q =new LinkedList<HNode>(); 
	        // Enqueue Root and initialize height
	        q.add(root);
 
	        while(true){
	            // nodeCount (queue size) indicates number of nodes
	            // at current level.
	            int nodeCount = q.size();
	            if(nodeCount == 0)
	                break;
	            // Dequeue all nodes of current level and Enqueue all
	            // nodes of next level
	            while(nodeCount > 0){
	                HNode node = q.peek();
	                System.out.print(node.c + " ");
	                q.remove();
	                if(node.left != null)
	                    q.add(node.left);
	                if(node.right != null)
	                    q.add(node.right);
	                nodeCount--;
	            }
	            System.out.println();
	        }
	}
	    
		
/**************************************** Encoding Methods ******************************/
	/* Constructs tree using keys and values from map 
	 * Returns the root
	 */
	public HNode constructTree(Map<Character, Integer> map){
		if(map.size() < 1){
			return null;
		}
		Comparator<HNode> compare_nodes = new Comparator<HNode>(){
			@Override 
			public int compare(HNode a, HNode b){
				return a.freq - b.freq;
			}
		}; 
		PriorityQueue<HNode> p_queue = new PriorityQueue<HNode>(map.size(), compare_nodes);
		map.forEach((k, v) -> p_queue.add( new HNode(k.charValue(), v.intValue(), null, null)));
		HNode n1, n2, temp, newNode;
		while(p_queue.size() > 1){
			n1 = p_queue.poll();
			n2 = p_queue.poll();
	/*		if(p_queue.size() > 0){
				temp = p_queue.peek();
				if((n2.freq == temp.freq) && (depth(temp)<depth(n2))){
					temp = p_queue.poll();
					p_queue.offer(n2);
					n2 = temp;	
				}
			}
	*/		newNode = new HNode('*', n1.freq+n2.freq, n1, n2);
			p_queue.offer(newNode);
		}
		return p_queue.poll();
	}
	

	/* Computes the binary number of a given integer */
	public String computeBinary(int x){
		String str = (x%2 == 0)? "0":"1";
		if(x < 2){
			return str;
		}
		return computeBinary(x/2) + str;
	}


	/* Returns 8 bit binary representation (String) of ASCII code */	
	public String asciiToBinary(int x){
		String s = computeBinary(x);
		while(s.length() < 8){
			s = "0" + s;
		}
		return s;
	}


	/* Creates the frequency file */
 	public void createFreqFile(String inputFile, String freqFile, Map<Character, Integer> map) throws FileNotFoundException{		
		/* Read characters from text file and store character count in frequency file */
			BinaryIn in = new BinaryIn(inputFile);
			char key;
			while(!in.isEmpty()){
				key = in.readChar();
				if(!map.containsKey(key)){
						map.put(key, 1);
				}	
				else{
					map.replace(key, map.get(key) + 1);
				}
			}
		

		/* Write contents of map to frequency file */
		try{	
			FileWriter file_writer = new FileWriter(freqFile);
			BufferedWriter buffer = new BufferedWriter(file_writer);
			map.forEach((k, v) ->
				{try{
					buffer.write(asciiToBinary((int)k) +":" + v);
					buffer.newLine();
				} catch(IOException e){
					System.out.println("Could not write to frequency file");
				}
			});
			buffer.close();	
		} catch(IOException e){
			e.printStackTrace();
			System.out.println("Could not write to frequency file");
		}	
	}
 


		
	/* Determines the code of each character as it trasverses a the tree */
	public void computeCharacterCode(Map<Character, String> map, HNode root, String str){
		if(root.isLeaf()){
			map.put(new Character(root.c), str);
			return;
		}
		computeCharacterCode(map, root.left, str+"1");
		computeCharacterCode(map, root.right, str+"0");
	}
	


	
	public void writeBinaryFile(String input_file, String binary_file, Map<Character, String> map){
		File file = new File(binary_file);
		BinaryIn in = new BinaryIn(input_file);
		BinaryOut out = new BinaryOut(binary_file);
		Character c;
		String str;
		while(!in.isEmpty()){
			c = new Character(in.readChar());
			if(map.containsKey(c)){
				str = map.get(new Character(c));
				for(int i = 0; i < str.length(); i++){
					out.write(str.charAt(i) == '1');
				}
			}
		}
		out.close();
	}


 
	public void encode(String inputFile, String outputFile, String freqFile){
		// TODO: Your code here
	  	Map<Character, Integer> freq_map = new HashMap<Character, Integer>();
		try{
			createFreqFile(inputFile, freqFile, freq_map);
		} catch(FileNotFoundException e){
			System.out.print("Could not open input file");	
		}		
		Map<Character, String> code_map = new HashMap<Character, String>(freq_map.size());
		computeCharacterCode(code_map, constructTree(freq_map), "");	
//		code_map.forEach((k, v) -> System.out.println(k + "\t" + v));
//		printLevelOrder(constructTree(freq_map));
		writeBinaryFile(inputFile, outputFile, code_map);
   }





/************************************ Decoding Methods *******************************************/
	
	public char convertBinaryToAscii(String s){
		int ascii = 0;
		for(int i = 0; i < s.length(); i++){
			if(s.charAt(s.length() - i -1) == '1'){
				ascii += (int)Math.pow(2, i);
			}
		}
		return (char)ascii;
	}

	public void readFreqFile(String freq_file, Map<Character, Integer> map){
		File file = new File(freq_file);
		BinaryIn in = new BinaryIn("freq.txt");
		String str_k, str_v = "";
		char c;
		Character key;
		while(!in.isEmpty()){
			str_k = "";
			str_v = "";
			for(int i = 0; i < 8; i++){
				str_k += String.valueOf(in.readChar());	
			}
			c = in.readChar(); //disregard :
			c = in.readChar();
			while(c != '\n'){
				str_v += String.valueOf(c);
				c = in.readChar();
			}
			//System.out.println("Key " + str_k + "    Value " + str_v);
			key = new Character(convertBinaryToAscii(str_k));
			map.put(key, Integer.valueOf(str_v));
		}	
	}


	public void computeCodeCharacter(Map<String, Character> map, HNode root, String str){
		if(root.isLeaf()){
			map.put(str, root.c);
			return;
		}
		computeCodeCharacter(map, root.left, str+"1");
		computeCodeCharacter(map, root.right, str+"0");
	}




	public void writeFile(String binary_file, String output_file, Map<String, Character> code_map){
		BinaryIn in = new BinaryIn(binary_file);
		BinaryOut out = new BinaryOut(output_file);
		String str;		
		while(!in.isEmpty()){
			str = "";
			while(!code_map.containsKey(str)){
				str += (in.readBoolean() == true)?"1":"0";
//				System.out.println("string = " + str);
				if(in.isEmpty()){
					break;
				}				
			}
			if(code_map.containsKey(str)){
				out.write(code_map.get(str));
			}
		}
		out.close();
	}



	public void decode(String inputFile, String outputFile, String freqFile){
		// TODO: Your code here
	  	Map<Character, Integer> freq_map = new HashMap<Character, Integer>();
	  	Map<String, Character> code_map = new HashMap<String, Character>();
		readFreqFile(freqFile, freq_map);
	 	computeCodeCharacter(code_map, constructTree(freq_map), "");	
//		code_map.forEach((k, v) -> System.out.println(v + "\t" + k));
//		printLevelOrder(constructTree(freq_map));
	 	writeFile(inputFile, outputFile, code_map);
   }







/************************************ Main ********************************************************/

   public static void main(String[] args) {
      		Huffman  huffman = new HuffmanSubmit();
		System.out.println("================== Huffman File Compression =============");
		if(args.length != 4){
			System.out.println("Usage");
			System.out.println("Encoding: java HuffmanSubmit \t <input file> \t <binary file> \t < frequency file> \t <0>");
			System.out.println("Decoding: java HuffmanSubmit \t <binary file> \t <output file> \t <frequency file> \t <1>");
			System.out.println("Ensure that the output file name is different from the name of an existing file you do not want to overwrite when decoding");					   
			System.exit(-1);	
		}
		
		if(Integer.valueOf(args[3]) == 0){
			System.out.println("Encoding...");
			huffman.encode(args[0], args[1], args[2]);
			System.out.println("Done encoding " + args[0] + "!");
		}
		else if(Integer.valueOf(args[3]) == 1){
			System.out.println("Decoding...");
			huffman.decode(args[0], args[1], args[2]);
			System.out.println("Done decoding. Files are ready to view!");
		}	
		else{
			System.out.println("Wrong mode.\n" + "Encoding: 0\n" + "Decoding: 1\n");
		}
		System.out.println("************************************************************");
   }

}
