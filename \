// Import any package as required
import java.util.*;
import java.io.*;



public class HuffmanSubmit implements Huffman {
  
	// Feel free to add more methods and variables as required.
	
	/* Used to construct tree */
	class HNode{
		char c;
		int freq;
		HNode left;
		HNode right;
		public HNode(char c, int freq, HNode left, HNode right){
			this.c = c;
			this.freq = freq;
 			this.left = left;
			this.right = right;
		} 

		public boolean isLeaf(){
			return ((left == null) && (right == null));
		}

	}
		
/**************************************** Encoding Methods ******************************/
	/* Constructs tree using keys and values from map 
	 * Returns the root
	 */
	public HNode constructTree(Map<Character, Integer> map){
		if(map.size() < 1){
			return null;
		}
		Comparator<HNode> compare_nodes = new Comparator<HNode>(){
			@Override 
			public int compare(HNode a, HNode b){
				return a.freq - b.freq;
			}
		}; 
		PriorityQueue<HNode> p_queue = new PriorityQueue<HNode>(map.size(), compare_nodes);
		map.forEach((k, v) -> p_queue.add( new HNode(k.charValue(), v.intValue(), null, null)));
		HNode n1, n2, newNode;
		while(p_queue.size() > 1){
			n1 = p_queue.poll();
			//System.out.println(n1.freq);
			n2 = p_queue.poll();
			//System.out.println(n2.freq);
			newNode = new HNode(' ', 0, n1, n2);
			p_queue.offer(newNode);
		}
		return p_queue.poll();
	}
	

	/* Computes the binary number of a given integer */
	public String computeBinary(int x){
		String str = (x%2 == 0)? "0":"1";
		if(x < 2){
			return str;
		}
		return computeBinary(x/2) + str;
	}


	/* Returns 8 bit binary representation of ASCII code */	
	public String asciiToBinary(int x){
		String s = computeBinary(x);
		while(s.length() < 8){
			s = "0" + s;
		}
		return s;
	}


	/* Creates the frequency file */
 	public void createFreqFile(String inputFile, String freqFile, Map<Character, Integer> map) throws FileNotFoundException{		
		/* Read characters from text file and store character count in frequency file */
		try{
			FileReader file_read = new FileReader(inputFile);
			BufferedReader buffer = new BufferedReader(file_read);
			String str;
			Character key;
			while((str = buffer.readLine()) != null){
				for(int i = 0; i < str.length(); i++){
					key = new Character(str.charAt(i));
					if(!map.containsKey(key)){
						map.put(key, 1);
					}	
					else{
						map.replace(key, map.get(key) + 1);
					}
				}	
			}
			buffer.close();
		} catch(IOException e){
			e.printStackTrace();
		}


		/* Write contents of map to file */
		try{	
			FileWriter file_writer = new FileWriter(freqFile);
			BufferedWriter buffer = new BufferedWriter(file_writer);
			map.forEach((k, v) ->
				{try{
					buffer.write(asciiToBinary((int)k) +":" + v);
					buffer.newLine();
				} catch(IOException e){
					System.out.println("Could not write to frequency file");
				}
			});
			buffer.close();	
		} catch(IOException e){
			e.printStackTrace();
			System.out.println("Could not write to frequency file");
		}	
	}
 


		
	
	public void computeCharacterCode(Map<Character, String> map, HNode root, String str){
		if(root.isLeaf()){
			map.put(new Character(root.c), new String(str));
			return;
		}
		if(root.left != null){
			str += "1";
			computeCharacterCode(map, root.left, str);
		}
		if(root.right != null){
			str += "0";
			computeCharacterCode(map, root.right, str);
		}
	}
	


	
	public void writeBinaryFile(String input_file, String binary_file, Map<Character, String> map){
		File file = new File(binary_file);
		BinaryIn in = new BinaryIn(input_file);
		BinaryOut out = new BinaryOut(binary_file);
		Character c;
		while(!in.isEmpty()){
			c = new Character(in.readChar());
		//	System.out.println(c);
			if(map.containsKey(c)){ 
				out.write(map.get(new Character(c)));
			}
		}
		out.close();
	}


 
	public void encode(String inputFile, String outputFile, String freqFile){
		// TODO: Your code here
	  	Map<Character, Integer> freq_map = new HashMap<Character, Integer>();
		try{
			createFreqFile(inputFile, freqFile, freq_map);
		} catch(FileNotFoundException e){
			System.out.print("Could not open input file");	
		}		
		Map<Character, String> code_map = new HashMap<Character, String>(freq_map.size());
		computeCharacterCode(code_map, constructTree(freq_map), "");	
		//code_map.forEach((k, v) -> System.out.println(v));
		writeBinaryFile(inputFile, outputFile, code_map);
   }





/************************************ Decoding Methods *******************************************/

	public char convertBinaryToAscii(String s){
		int ascii = 0;
		for(int i = 0; i < s.length; i++){
			ascii += Integer.parseInt(s.charAt(s.length - i - 1)) * (int)Math.pow(2, i);
		}
		return (char)ascii;
	}

	public void readFreqFile(String freq_file, Map<Character, Interger> map){
		File file = new File(freq_file);
		BinaryIn in = new BinaryIn(freq_file);
		String str_k, str_v = "";
		Character key;
		while(!in.isEmpty()){
			for(int i = 0; i < 8; i++){
				str_k += Character.toString(in.readBolean());	
			}
			in.readChar(); //ignore :
			str_v = "";
			while(in.readChar() != ' '){
				str_v += Character.toString(in.readChar());
			}
			key = new Character(convertBinaryToAscii(str_k));
			map.put(key, Integer.parseInteger(str_v));
		}	
	}


	public void computeCodeCharacter(Map<String, Character> map, HNode root, String str){
		if(root.isLeaf()){
			map.put(str, root.c);
			return;
		}
		computeCodeCharacter(map, root.left, str+"1");
		computeCodeCharacter(map, root.right, str+"0");
	}




	public void writeFile(String binary_file, String output_file, Map<String, Character> code_map){
		Binary in = new BinaryIn(binary_file);
		String str = "";		
		/* Write contents of map to file */
		try{	
			FileWriter file_writer = new FileWriter(freqFile);
			BufferedWriter buffer = new BufferedWriter(file_writer);
			while(!in.isEmpty()){
				while(!code_map.containsKey(str)){
					if(in.readBoolean()){
						str += "1";
					}
					else{
						str += "0";
					}
				}
				buffer.write(map.get(str));	
			}
			buffer.close();	
		} catch(IOException e){
			e.printStackTrace();
			System.out.println("Could not write to frequency file");
		}	
		
	}



	public void decode(String inputFile, String outpuFile, String freqFile){
		// TODO: Your code here
	  	Map<Character, Integer> freq_map = new HashMap<Character, Integer>();
	  	Map<String, Character> code_map = new HashMap<String, Characterr>();
		try{
			readFreqFile(freqFile, freq_map);
		} catch(FileNotFoundException e){
			System.out.print("Could not open input file");	
		
		}	
		 computeCodeCharacter(code_map, constructTree(freq_map), "");	
		 writeFile(inputFile, outputFile, code_map);
			
		
   }







/************************************ Main ********************************************************/

   public static void main(String[] args) {
      		Huffman  huffman = new HuffmanSubmit();
		huffman.encode("alice30.txt", "ur.enc", "freq.txt");
		//huffman.encode("ur.jpg", "ur.enc", "freq.txt");
		huffman.decode("ur.enc", "ur_dec.jpg", "freq.txt");
		// After decoding, both ur.jpg and ur_dec.jpg should be the same. 
		// On linux and mac, you can use `diff' command to check if they are the same. 
   }

}
