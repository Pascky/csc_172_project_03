JFLAGS = -Xlint
JC = javac
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
        BinaryOut.java \
        BinaryIn.java \
        Huffman.java \
        HuffmanSubmit.java 

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class

